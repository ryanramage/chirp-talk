define(["Ractive", "ractive-transitions-typewriter"], function(Ractive, tw){

  var stopped = false;
  var myAudio = new Audio('text.wav');
  myAudio.addEventListener('ended', function() {
      this.currentTime = 0;
      if (!stopped) this.play();
      else console.log('done')
  }, false);
  myAudio.play();


  var interval;
  function endAudio(){
    var vol = myAudio.volume;
    if (vol < 0.1 || stopped) clearInterval(interval);
    myAudio.volume = vol / 4;
  }

  return {

    initialize: function() {
      ractive = new Ractive({
        el: 'demo',
        template: '#demo-template',
        data: { visible: true },
        complete: function () {
          stopped = true;
          setTimeout(function(){
            setInterval(endAudio, 50);
          }, 100);

        }
      });

      console.log('init');
    }
  }
});
