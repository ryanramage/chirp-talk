var jam = {
    "packages": [
        {
            "name": "Ractive",
            "location": "jam/ractive",
            "main": "Ractive.js"
        },
        {
            "name": "ractive-transitions-typewriter",
            "location": "jam/ractive-transitions-typewriter",
            "main": "Ractive-transitions-typewriter.js"
        }
    ],
    "version": "0.2.17",
    "shim": {}
};

if (typeof require !== "undefined" && require.config) {
    require.config({
    "packages": [
        {
            "name": "Ractive",
            "location": "jam/ractive",
            "main": "Ractive.js"
        },
        {
            "name": "ractive-transitions-typewriter",
            "location": "jam/ractive-transitions-typewriter",
            "main": "Ractive-transitions-typewriter.js"
        }
    ],
    "shim": {}
});
}
else {
    var require = {
    "packages": [
        {
            "name": "Ractive",
            "location": "jam/ractive",
            "main": "Ractive.js"
        },
        {
            "name": "ractive-transitions-typewriter",
            "location": "jam/ractive-transitions-typewriter",
            "main": "Ractive-transitions-typewriter.js"
        }
    ],
    "shim": {}
};
}

if (typeof exports !== "undefined" && typeof module !== "undefined") {
    module.exports = jam;
}